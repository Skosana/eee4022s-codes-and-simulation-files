To run the MILP optimizatation algorithm, please follow these few steps.
1. The main file is MILP_main
2. Set the same 'nNodes' variable in the MILP_main and parentnode files. nNodes = 16 for the 16 bus system and 33 for 33-bus system.
3. Keep all the Matlab in one folder and have them all open on matlab before running MILP_main (MILP_main.m, P_equality.m, Q_equality.m, firstDG.m, parentnode.m, secondDG.m and selectbranch.m)


 For the Simulink models
 Use IEEE_16Bus_System_4022S_.slx for the 16-bus and SKSSIY002_33_BUS_HEURISTICc.slx for the 33-bus system.
 To open switch, comment out the connection of the like you want to open and uncomment to close switch.
 To run the loadflow analyzer, doubleclick the powergui button and scroll to load flow analyzer then click compute.
	
