%  ParentNode file
function Aa= parentnode(mg,T)
T1=dfsearch(T,mg,'edgetonew'); %returns an ordered cell array of egdes from a microgrid.N-by 2
v1=dfsearch(T,mg);% node ID in terms of their discovery from MG 33x1
SourceNode=T1(:,1);
DestinationNode=T1(:,2);

nNodes = 33; %33 for ieee33 MAKE SURRE IT IS THE SANE AS nNODES SET IN MILP_main FILE

n1=nNodes-1;
n2=nNodes;
Aa=zeros(n1,n2);
s=0;
for i=1:size(SourceNode)
    from=find(SourceNode==v1(i)); %node position of nodes in V1 and fr in order
    for k=1:size(from,1)
        s=s+1;
        Aa(s,v1(i))=-1; %parent nodes
        Aa(s,DestinationNode(from(k)))=1;% children nodes looks for nodes connected to frm
    end
end
Aa;
end
 