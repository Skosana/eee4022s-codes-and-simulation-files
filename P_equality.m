% Active Power equality constraints code File
function [P1eq,P11eq]=P_equality(dg,powerdata,insert,G)  
      
    T1= dfsearch(G,dg,'edgetonew');
    SourceNode=T1(:,1);
    DestinationNode=T1(:,2); 	
    v1=dfsearch(G,dg);    
 
for k=size(v1):-1:1    	
         a=v1(k);
         
         %Form the Pineq matrix for nodes other than that involved in loop formation because the node within loop will have different one...
        
         	P1(a)= powerdata(a,2);
         	P1eq(insert,a)=1;
         	P11eq(insert,a)= -1*P1(a);
         	add=find(SourceNode==v1(k));
         	for m=1:size(add,1) 
                  P1eq(insert,DestinationNode(add(m)))=-1;              	
         	end
             insert=insert+1;       	
      	
end
end