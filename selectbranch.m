function branch = selectbranch(nNodes)


%TIE SWITCHES INCLUDED
%                   Source bus      Destination bus

        branch16 = [1                       4;
                    2                       8;
                    3                      13;
                    4                       5;
                    4                       6;
                    5                      11;
                    6                       7;
                    7                      16;
                    8                      10;
                    8                       9;
                    9                      12;
                    9                      11;
                    10                     14;
                    13                     14;
                    13                     15;
                    15                     16
                    5                      11
                    10                     14
                    7                      16];
                
        branch33 = [1                       2;
                    2                       3;
                    3                       4;
                    4                       5;
                    5                       6;
                    6                       7;
                    7                       8;
                    8                       9;
                    9                      10;
                    10                     11;
                    11                     12;
                    12                     13;
                    13                     14;
                    14                     15;
                    15                     16;
                    16                     17;
                    17                     18;
                    2                      19;
                    19                     20;
                    20                     21;
                    21                     22;
                    3                      23;
                    23                     24;
                    24                     25;
                    6                      26;
                    26                     27;
                    27                     28;
                    28                     29;
                    29                     30;
                    30                     31;
                    31                     32;
                    32                     33
                    18                     33
                    12                     22
                    8	                   21
                    9 	                   15
                    25                     29];

        
switch nNodes
    
case 16
branch = branch16;

case 33
branch = branch33;
end