	% First DG Codes File
function [ dg_insP1, dg_insQ1, dg_insv1]=firstDG(DG1,powerdata,path1,nNodes)
    %****Real Power Euality Constraints***********
   
    
     T1= dfsearch(path1,DG1,'edgetonew');
     v1=dfsearch(path1,DG1);
     SourceNode=T1(:,1);  
     DestinationNode=T1(:,2);   
     size(v1);
    %Calculate cummulative active power of "active" nodes based on DG .
    %Later these values are used to calculate the voltage at the nodes with
    insert=1;  	
    [P1eq,P11eq]=P_equality(DG1,powerdata,insert,path1);
    [Q1eq,Q11eq]=Q_equality(DG1,powerdata,insert,path1);
 
    %Voltage values according to path 1 needs to be calculated as follows,
    Volt1=zeros(nNodes,nNodes);
    Vp1=zeros(nNodes,nNodes);
    Vq1=zeros(nNodes,nNodes);
    n=size((v1),1)-1;
    Volt1(1,DG1)=1; %%Voltage at position dg1 is 1 p.u
    for i=2:size(v1)    	
        Volt1(i,SourceNode(n))=1;
        Volt1(i,DestinationNode(n))=-1;
        Vp1(i,DestinationNode(n))=-0.001/100;
        Vq1(i,DestinationNode(n))=-0.001/100;
        n=n-1;
    end 
    
    %Inserting zeros (z1), z2 for path #2 and z3 for final power calculation
    %for the node assignment variable to make the matrix
    %equal to size of f
   
  
    z=zeros(nNodes,nNodes);
 
    dg_insP1=[P11eq z P1eq z z z z z];
    dg_insQ1=[Q11eq z z z Q1eq z z z];
    dg_insv1=[z z Vp1 z Vq1 z Volt1 z];
end