function MILP_main
tic
     
 clc; 
         nNodes=33; %33 for IEEE 33 bus ---- MUST MATCH nNodes ON 'parentnode' CODE THERE'S A PLACE YOU MUST CHANGE nNodes
        
        
     
       
        
        powerdata = selectpower(nNodes);
        edges = selectbranch(nNodes);
  
        SourceNode=edges(:,1);
        DestinationNode=edges(:,2);	
        
        G = graph(SourceNode,DestinationNode);
        Original_graph = minspantree(G);
        G_with_faults = G;
        

        
        % Removing faulted lines ---- CREATE OUTage LINES FOR IEEE33 AND ADD
        %IF IEEE 16-BUS IS COMMENTED, UNCOMMENT 33-BUS, VICE VERSA
        
        
 %UNCOMMENT TO USE IEEE 33-BUS
        DG1=11;
        DG2=29;
        G_with_faults=rmedge(G_with_faults,2,19);
        G_with_faults=rmedge(G_with_faults,8,9);
        G_with_faults=rmedge(G_with_faults,16,17);
        G_with_faults=rmedge(G_with_faults,31,32);
% 
% 
%UNCOMMENT TO USE IEEE 16-BUS

%         G_with_faults=rmedge(G_with_faults,4,6);
%         G_with_faults=rmedge(G_with_faults,3,13);
%               
% 
%           DG1=6;
%           DG2=9;




        T = minspantree(G_with_faults); %reconfiguring using mst
   
 

  positionDG1 = DG1;
  positionDG2 = DG2;
        
        %Inserting new variables for power calculations
   
        %Now start including the power (real and reactive and voltage
        %variables) v1,P1,Q1,V1
        %DERs
        f1= ones(nNodes,1);
        f0=zeros(nNodes,1);
        
   
        %Combining all the variables for objective function matrices..
        f=-1*[f1; f1; f0 ;f0 ;f0 ;f0 ;f0; f0;0;0];	%We want minimum, hence multiplication by -1
       
       	
        %Inequality constraints for a node belonging to only one dg
        A_inequality1=zeros(nNodes,8*nNodes+2);
        b_inequality1=[ones(nNodes,1)];
        for i=1:nNodes
          	A_inequality1(i,i)=1;
          	A_inequality1(i,i+nNodes)=1;
        end
      
         [A_inequality2]=parentnode(DG1,T);
         
         b_inequality2=zeros(nNodes-1,1);
         
         
        %             dg2
         [A_inequality3]=parentnode(DG2,T);
         b_inequality3=zeros(nNodes-1,1);
         zero=zeros(size(A_inequality2));
       
         
        %adding the matrices correctly including the LS variable
         [A_inequality2]=[A_inequality2 zero zero zero zero zero zero zero zeros(nNodes-1,2)];
         [A_inequality3]=[zero A_inequality3 zero zero zero zero zero zero zeros(nNodes-1,2)];
         A_inequality=[A_inequality1;A_inequality2;A_inequality3];
         b_inequality=[b_inequality1;b_inequality2;b_inequality3];
 
        
      	%Calculating power and Aeq bineq will be updated as required for the
      
      
        [ dg_insP11, dg_insQ11, dg_insv11]= firstDG(DG1,powerdata,Original_graph,nNodes);
        [ dg_insP21, dg_insQ21, dg_insv21]= secondDG(DG2,powerdata,Original_graph,nNodes);
        
 
 
    
        % Node where DG is installed is always one.
        A_equality1=zeros(2,size(f,1));
      
       
        A_equality1(1,DG1)=1;
        A_equality1(2,DG2+nNodes)=1;
        A_equality=A_equality1;
        b_equality=[1;1];
        
       
        A_equality=[A_equality;dg_insP11 zeros(nNodes,2) ;dg_insP21 zeros(nNodes,2) ;dg_insQ11 zeros(nNodes,2) ;dg_insQ21 zeros(nNodes,2) ;dg_insv11 zeros(nNodes,2) ;dg_insv21 zeros(nNodes,2)];
        b_equality=[b_equality;zeros(4*nNodes,1);1;zeros(nNodes-1,1);1;zeros(nNodes-1,1)]; %add 1 to the voltage node
 
    %      Finding the total power for each DER based on node-DER assignment
    %      variable.
        A_equality1=[ powerdata(:,2)' zeros(1,7*nNodes) -1 0 ];
        A_equality2=[zeros(1,nNodes) powerdata(:,2)' zeros(1,6*nNodes) 0 -1 ];
 
        A_equality=[A_equality;A_equality1;A_equality2];
        b_equality=[b_equality;0;0];
 
       % LOWER AND UPPER BOUND CONSTRAINTS
        
        lb=zeros(2*nNodes,1);
        ub=ones(2*nNodes,1);
        lb=[lb;-3800*ones(2*nNodes,1);-1900*ones(2*nNodes,1) ;.95*ones(2*nNodes,1);0;0];
        ub=[ub ;3800*ones(2*nNodes,1);2000*ones(2*nNodes,1) ;1.05*ones(2*nNodes,1);2500;2000];
       
            	
 

        [x,fval] = linprog(f,A_inequality,b_inequality,A_equality,b_equality,lb,ub);
disp('----------------------------------------------------------------------------------------------------------------')
disp(' SIMULATION RESULTS OF LOAD RESTORATION OF AN IEEE33 BUS DISTRIBUTION NETWORK  USING DG ISLANDS and tie switches')
disp('----------------------------------------------------------------------------------------------------------------')
 
        fprintf ('Number of nodes active in DG = %f\n', -1*fval);
        disp ('Index =');
        
        disp (x');

        active=find(x); 
       
        size(active);
 
        DG1=[];
        DG2=[];
      
        for i=1:size(active)
        	if active(i)<nNodes
            	DG1=[DG1 active(i)];
        	end
        	if active(i)>=nNodes && active(i)<=2*nNodes+1
           	if active(i)==nNodes
              	active(i)=nNodes;
           	else
               	active(i)=active(i)-nNodes;
                	
               end 
           	
                	DG2=[DG2 active(i)];
           	
        	end
 
        end
        
        

 
disp('-----------------------------------------------------------------------------------------------------------------')
disp(['  Location of DGs   	DG1 : Node  ', num2str(positionDG1), '     	DG2 : Node ', num2str(positionDG2),''])
disp(['  Total Power Supplied (MW)  	        DG1 :   ', num2str(x(8*nNodes+1)), '     	DG2 : ', num2str(x(8*nNodes+2)),''])
disp('-----------------------------------------------------------------------------------------------------------------')
disp(['  Nodes supplied by DG1	', num2str(DG1),'          	'])
disp('-----------------------------------------------------------------------------------------------------------------')
disp(['  Nodes supplied by DG2	', num2str(DG2),'                       	']) 
disp('-----------------------------------------------------------------------------------------------------------------')
disp(['  Total Power supplied by DGs to all the nodes (MW)	: ',num2str(x(8*nNodes+1)+ x(8*nNodes+2)),''])
    	
        	
        toc
    end