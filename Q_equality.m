%  Reactive Power equality constraints code File
function [Q1eq,Q11eq]=Q_equality(dg,power,insert,G)  
 
    T1= dfsearch(G,dg,'edgetonew');
    SourceNode=T1(:,1);
    DestinationNode=T1(:,2); 	
    v1=dfsearch(G,dg);

for k=size(v1):-1:1    	
         a=v1(k);
         %Form the Qineq matrix for nodes other than that involved in loop formation because the node within loop will have different one...
        
         	Q1(a)=power(a,3);
         	Q1eq(insert,a)=1;
         	Q11eq(insert,a)=-1*Q1(a);
         	add=find(SourceNode==v1(k));
         	for m=1:size(add,1) 
                  Q1eq(insert,DestinationNode(add(m)))=-1;              	
         	end
         	insert=insert+1; 
        
      	
end
end
 